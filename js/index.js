/*-------------------------Bài 1------------------------------- */
domID("btnketquadiem").onclick = function () {
  // input: number
  var nhapDiemChuan = domID("nhapDiemChuan").value * 1;

  var khuVuc = domID("khuVuc").value * 1;

  var doiTuong = domID("doiTuong").value * 1;

  var nhapDiem1 = domID("nhapDiemMon1").value * 1;

  var nhapDiem2 = domID("nhapDiemMon2").value * 1;

  var nhapDiem3 = domID("nhapDiemMon3").value * 1;
  //output string
  var ketQua = "";
  //progress
  var tongDiem = nhapDiem1 + nhapDiem2 + nhapDiem3 + doiTuong + khuVuc;
  if (
    nhapDiem1 >= 0 &&
    nhapDiem1 <= 10 &&
    nhapDiem2 >= 0 &&
    nhapDiem2 <= 10 &&
    nhapDiem3 >= 0 &&
    nhapDiem3 <= 10
  ) {
    if (nhapDiem1 === 0 || nhapDiem2 === 0 || nhapDiem3 === 0) {
      ketQua = "Có môn 0 điểm, bạn đã rớt";
    } else if (tongDiem < nhapDiemChuan) {
      var diemThieu = nhapDiemChuan - tongDiem;
      ketQua = `Bạn đã rớt do điểm của bạn ít hơn điểm chuẩn là ${diemThieu} điểm`;
    } else if (tongDiem >= nhapDiemChuan) {
      ketQua = `Bạn đã đậu, chúc mừng bạn`;
    }
  } else {
    ketQua = "Bạn vui lòng nhập đúng số điểm";
  }
  document.getElementById("ketQua").innerHTML = ketQua;
};
/*-----------------------Bài 2 ------------------------------ */
domID("btntinhtiendien").onclick = function () {
  //input: string, number
  var nhapHoTen = domID("nhapHoten").value;
  var nhapSoDien = domID("nhapSoDien").value * 1;
  //output: string,number
  var tienDien = 0;
  var ketQuaTienDien = "";
  //progress
  if (nhapHoTen != 0) {
    if (nhapSoDien >= 0) {
      if (nhapSoDien <= 50) {
        tienDien = tiendien(nhapSoDien, 500);
      } else if (nhapSoDien <= 100) {
        tienDien = tiendien(nhapSoDien - 50, 650) + tiendien(50, 500);
      } else if (nhapSoDien <= 200) {
        tienDien =
          tiendien(nhapSoDien - 100, 850) +
          tiendien(50, 650) +
          tiendien(50, 500);
      } else if (nhapSoDien <= 350) {
        tienDien =
          tiendien(nhapSoDien - 200, 1100) +
          tiendien(100, 850) +
          tiendien(50, 650) +
          tiendien(50, 500);
      } else {
        tienDien =
          tiendien(nhapSoDien - 350, 1300) +
          tiendien(150, 1100) +
          tiendien(100, 850) +
          tiendien(50, 650) +
          tiendien(50, 500);
      }
      ketQuaTienDien = ` số tiền điện của ${nhapHoTen} là ${tienDien.toLocaleString()} đồng`;
    } else {
      alert("vui lòng nhập đúng số điện");
    }
  } else {
    alert("vui lòng nhập họ tên");
  }
  domID("ketQuaTienDien").innerHTML = ketQuaTienDien;
  function tiendien(sodien, sotien) {
    var tienDienTieuThu = Number(sodien * sotien);
    return tienDienTieuThu;
  }
};
/*---------------------Bài 3 --------------------------------- */
domID("btnTinhThue").onclick = function () {
  //input: number, string
  var nhapHoTen = domID("nhapTen").value;
  var nhapThuNhap = domID("nhapTongThuNhap").value * 1;
  var nhapSoNguoi = domID("soNguoiPhuThuoc").value * 1;
  //output: number
  ketQuaThue = 0;
  //progress
  var thuNhapChiuThue = thunhapchiuthue(nhapThuNhap, nhapSoNguoi);
  if (nhapSoNguoi >= 0 && nhapThuNhap >= 0) {
    ketQuaThue = ketquathue(thuNhapChiuThue);
  } else {
    alert(
      "Số người nhập không được bé hơn 0 người và thuế thu nhập không được bé hơn 0"
    );
  }
  domID(
    "ketQuaThue"
  ).innerHTML = `Tiền thuế phải chịu của ${nhapHoTen} là ${ketQuaThue.toLocaleString()} đồng`;

  function thunhapchiuthue(thunhap, songuoi) {
    var tong = thunhap - 4e6 - songuoi * 1.6e6;
    return tong;
  }

  function ketquathue(a) {
    var ketQua = 0;
    if (a <= 60e6) {
      ketQua = a * 0.05;
    } else if (a <= 120e6) {
      ketQua = 60e6 * 0.05 + (a - 60e6) * 0.1;
    } else if (a <= 210e6) {
      ketQua = 60e6 * 0.05 + 60e6 * 0.1 + (a - 120e6) * 0.15;
    } else if (a <= 384) {
      ketQua = 60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (a - 210e6) * 0.2;
    } else if (a <= 624) {
      ketQua =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        (a - 384e6) * 0.25;
    } else if (a <= 960) {
      ketQua =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        (a - 624e6) * 0.3;
    } else {
      ketQua =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        336e6 * 0.3 +
        (a - 960e6) * 0.35;
    }
    return ketQua;
  }
};
